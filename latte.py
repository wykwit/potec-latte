from copy import deepcopy

# parse the truth table
def parse(input_string, separator="\t"):
	F, R = [], []
	for line in input_string.split("\n"):
		line = line.strip().split(separator)
		if line[-1] == "1":
			F.append([int(x) for x in line[0]])
		elif line[-1] == "0":
			R.append([int(x) for x in line[0]])
		else:
			continue
	return F, R

def minimal_coverage(X):
	r = []
	for w in range(1, len(X[0])+1):
		for x in range(len(X[0])):
			c = 0
			for y in range(len(X)):
				a = 0
				for i in range(w):
					a |= X[y][(x+i)%len(X[0])]
				c += a
			if c == len(X):
				r.append([x, w])
		if len(r) != 0:
			break
	return r

def implicants(k, R, verbose=False):
	if verbose: print("k:", k)
	B = deepcopy(R)
	for x in range(len(k)):
		if k[x] == 1:
			for y in range(len(B)):
				B[y][x] = 1 - B[y][x]
	if verbose:
		print("Block matrix:")
		for r in B:
			print(r)
	L = minimal_coverage(B)
	if verbose:
		print("Minimal coverage:")
		for r in L:
			print(r)
	r = []
	for i in range(len(L)):
		I = [2]*len(k)
		for j in range(L[i][1]):
			index = (L[i][0]+j)%len(k)
			I[index] = k[index]
		r.append(I.copy())
	if verbose:
		print("Implicants:")
		for _r in r:
			print(_r)
	return r

# return all implicants in a flat list
def flat_implicants(F, R, verbose=False):
	i = []
	for k in F:
		i += implicants(k, R, verbose=verbose)
		if verbose: print("")
	return i

# return only unique elements from a list
def unique(t):
	r = []
	for x in t:
		if x not in r:
			r.append(x)
	return r

def check_implicant(k, i):
	assert len(k) == len(i)
	matching = True
	for j in range(len(i)):
		if i[j] != 2 and i[j] != k[j]:
			matching = False
			break
	return matching

def matrix_for_function(F, I):
	r = [[int(check_implicant(k, i)) for i in I] for k in F]
	return r

def check_coverage(C):
	# C is a list of columns
	for y in range(len(C[0])):
		a = 0
		for x in range(len(C)):
			a |= C[x][y]
		if a != 1:
			return False
	return True	

def pick_columns(i, n, w, r):
	# i - our index
	# n - the ammount of columns
	# w - desired precise width
	# r - returned data
	if i == n: return []
	r += [i]
	if len(r) == w: return [r]
	out = []
	for j in range(i+1, n):
		out += pick_columns(j, n, w, deepcopy(r))
	return out

def produce_columns(X, pick):
	return [[X[i][p] for i in range(len(X))] for p in pick]

def systematic_coverage(X):
	r = []
	n = len(X[0])
	for w in range(1, n+1):
		picks = []
		for i in range(n):
			picks += pick_columns(i, n, w, [])
		for pick in picks:
			columns = produce_columns(X, pick)
			if check_coverage(columns):
				r.append(pick)
		if len(r) != 0:
			break
	return r

def systematic_solution(input_string, verbose=False, formula_prefix=""):
	F, R = parse(input_string)
	if verbose:
		print("F (one-set):")
		for r in F:
			print(r)
		print("R (zero-set):")
		for r in R:
			print(r)
		print("")
	I = unique(flat_implicants(F, R, verbose=verbose))
	if verbose:
		print("All unique implicants:")
		for r in I:
			print(r)

	m = matrix_for_function(F, I)
	if verbose:
		print("Matrix of implicants for our function:")
		for r in m:
			print(r)
		print("Minimal functions:")
	r = []
	#for x in minimal_coverage(m):
	for x in systematic_coverage(m):
		if verbose: print("Coverage:", x)
		row = []
		for i in x:
			row.append(I[i])
			if verbose: print(row[-1])
		if verbose: print(pretty_formula(row, prefix=formula_prefix))
		r.append(row)
	return r

def heuristic_solution(input_string, verbose=False, formula_prefix=""):
	F, R = parse(input_string)
	if verbose:
		print("F (one-set):")
		for r in F:
			print(r)
		print("R (zero-set):")
		for r in R:
			print(r)
		print("")
	I = []
	for k in F:
		if [check_implicant(k, i) for i in I].count(True) > 0:
			continue
		I.append(implicants(k, R, verbose=verbose)[0])
		if verbose:
			print("We're choosing only the first implicant.")
			print(I[-1])
			print("")
	if verbose:
		print("Heuristic implicants:")
		for r in I:
			print(r)
		print(pretty_formula(I, prefix=formula_prefix))
	return r

def pretty_formula(I, prefix=""):
	s = ""
	for i in I:
		if s != "":
			s += " + "
		for j in range(len(i)):
			if i[j] != 2: s += chr(ord("a")+j)
			if i[j] == 0: s += "'"
	return prefix + s
				

if __name__ == "__main__":
	input_string = """
0000	1
0001	1
0010	0
0011	1
0100	0
0101	-
0110	0
0111	1
1000	1
1001	0
1100	1
1111	0
"""

	verbose = True
	s = systematic_solution(input_string, verbose)

