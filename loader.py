import latte

fname = "1-table.txt"

inputs = []
for i in range(2, 8+1):
	with open(fname.replace("1", str(i)), "r") as f:
		d = f.read().replace(" ", "\t")
		inputs.append(d)

for x in inputs:
	function_name = x.split()[1].upper()
	print("Systematic solution for function "+function_name+":")
	latte.systematic_solution(x, True, function_name+" = ")
	print("\n")

for x in inputs:
	function_name = x.split()[1].upper()
	print("Heuristic solution for function "+function_name+":")
	latte.heuristic_solution(x, True, function_name+" = ")
	print("\n")
